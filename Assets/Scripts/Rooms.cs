﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Types of the room states.
public enum RoomStates
{
    Empty,
    Full
}

// The class handles the room information from the server.
public class Rooms : MonoBehaviour
{

    // One room represents one game on the server.
    public struct Room
    {
        public int roomId;
        public string roomName;
        public RoomStates roomState;
    }

    // Objects.
    public GameObject scrollViewContents;
    public GameObject roomButtonPrefab;
    public GameObject serverConnectionObject;
    public GameObject roomNameLabel;
    public GameObject roomFullWarning;
    public GameObject mainMenuControllerObject;

    private MainMenuController mainMenuController;
    private ServerConnection serverConnection;
    private Room[] rooms;
    private int selectedRoom;
    private bool isRoomSelected;

    private List<GameObject> roomButtonsList = new List<GameObject>();

    private void Start()
    {
        serverConnection = ServerConnection.Instance;
        mainMenuController = mainMenuControllerObject.GetComponent<MainMenuController>();
        isRoomSelected = false;
        roomFullWarning.SetActive(false);
    }

    // Room data format: "id_state_name_id2_state2_name2.....".
    // state: e - empty, f - full.
    public void ProcessRoomsString(string roomsData)
    {
        roomFullWarning.SetActive(false);
        string[] words = roomsData.Split('_');
        int roomsCount = words.Length / 3;  // / 3 to determine the number of rooms.
        rooms = new Room[roomsCount];

        for (int i = 0; i < rooms.Length; i++)
        {
            rooms[i].roomId = int.Parse(words[i * 3 + 1]); // i * 3 to address the ID of the room in the words array.

            char stateChar = Convert.ToChar(words[i * 3 + 2]); // i * 3 + 2 to address the state symbol of the room.

            rooms[i].roomName = words[i * 3 + 3]; // i * 3 + 1 to address the room name in the words array.

            GameObject button = Instantiate(roomButtonPrefab, scrollViewContents.transform);

            if (stateChar.Equals('e'))
            {
                rooms[i].roomState = RoomStates.Empty;
                button.GetComponent<Image>().color = new Color(0, 255f, 0); // Grenn button for empty room.
            }
            else if (stateChar.Equals('f'))
            {
                rooms[i].roomState = RoomStates.Full;
                button.GetComponent<Image>().color = new Color(255f, 0, 0); // Red button for full room.
            }

            button.transform.GetComponentInChildren<TextMeshProUGUI>().text = rooms[i].roomName;
            int roomIndex = i;
            button.GetComponent<Button>().onClick.AddListener(() => SelectRoom(rooms[roomIndex]));
            roomButtonsList.Add(button);
        }
    }

    // Message used to join room.
    public string JoinRoomMessage()
    {
        string message = "JoinRoom_" + selectedRoom + "_" + serverConnection.playerName;
        return message;
    }

    // If the join room button is clicked.
    public void OnJoinRoomButtonClicked()
    {
        if (isRoomSelected == true)
        {
            JoinRoom();
        }
    }

    public void DeleteRoomInfo()
    {
        roomButtonsList.Clear();
        foreach (Transform child in scrollViewContents.transform)
        {
            Destroy(child.gameObject);
        }
    }

    // Join selected room.
    private void JoinRoom()
    {
        mainMenuController.menuState = MainMenuController.MenuState.WaitingForJoin;
        string message = JoinRoomMessage();
        serverConnection.SendToServer(message);
    }

    public void TransferToRoom()
    {
        SceneManager.LoadScene("Game");
    }

    public void ShowWarning()
    {
        roomFullWarning.SetActive(true);
    }

    // Selects room from the scroll view.
    public void SelectRoom(Room room)
    {
        if (room.roomState != RoomStates.Full)
        {
            selectedRoom = room.roomId;
            isRoomSelected = true;
            roomNameLabel.GetComponent<TextMeshProUGUI>().text = room.roomName;
            roomFullWarning.SetActive(false);
            Debug.Log("SelectedRoom " + room.roomName);
        }
        else
        {
            roomFullWarning.SetActive(true);
        }
    }

}
