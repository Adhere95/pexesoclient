﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

//The class controls main menu UI.
public class MainMenuController : MonoBehaviour
{
    // Objects
    public GameObject connectMenuPanel;
    public GameObject mainMenuPanel;
    public GameObject createRoomPanel;
    public GameObject joinRoomPanel;
    public GameObject disconnectedPanel;
    public GameObject nameWarning;
    public GameObject playerNameInputField;
    public GameObject roomNameInputField;
    public GameObject serverConnectionObject;
    public GameObject roomsScrollView;
    public GameObject roomNameWarning;
    public GameObject connectedLabel;
    public GameObject notConnectedLabel;
    public GameObject disconnectedLabel;
    public GameObject connectionLabel;
    public GameObject panelReconnect;

    // Server connection
    private ServerConnection serverConnection;

    // Forbidden character in the player's name. It is used to separate parts of messages in TCP protocol.
    private char forbiddenChar = '_';

    // Maximum length of names.
    private int nameMaxLength = 12;

    // Names of the player and the room if it is created.
    public string PlayerName { get; set; }
    public string RoomName { get; set; }

    // States of the menu.
    public enum MenuState
    {
        StartState, WaitingForConnection, WaitingForCreate, WaitingForInfo, WaitingForJoin, Disconnected, ConnectionLost
    }
    public MenuState menuState;

    // Start is called before the first frame update
    // Initialization of the variables.
    void Start()
    {
        serverConnection = ServerConnection.Instance;

        // Main menu initialization.
        connectMenuPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
        createRoomPanel.SetActive(false);
        joinRoomPanel.SetActive(false);
        disconnectedPanel.SetActive(false);
        nameWarning.SetActive(false);
        roomNameWarning.SetActive(false);
        connectedLabel.SetActive(false);
        notConnectedLabel.SetActive(true);
        disconnectedLabel.SetActive(false);
        connectionLabel.SetActive(false);
        panelReconnect.SetActive(false);

        if (serverConnection.showReconnect == true)
        {
            panelReconnect.SetActive(true);
            serverConnection.showReconnect = false;
        }

        menuState = MenuState.StartState;
        if (serverConnection.completed == true)
        {
            TransferToMainMenu();
            notConnectedLabel.SetActive(false);
            connectedLabel.SetActive(true);
        }
    }

    // The method checks if the player name match given conventions.
    public bool CheckNameValidity(string name)
    {
        if (name != null && name.Equals("") == false && name.Contains(forbiddenChar) == false && name.Length <= nameMaxLength)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // If the Connect button in the connection panel is clicked. Connect and change the menu.
    public void OnConnectButtonClicked()
    {
        if (CheckNameValidity(serverConnection.playerName) == true)   // If the name if valid.
        {
            nameWarning.SetActive(false);
            menuState = MenuState.WaitingForConnection;
            serverConnection.ConnectToServer(); // Connect to the server.
        }
        else
        {
            nameWarning.SetActive(true);
        }
    }

    // Transfer from connect menu to main menu.
    public void TransferToMainMenu()
    {
        connectMenuPanel.SetActive(false);  // Transfer to the main menu.
        mainMenuPanel.SetActive(true);
    }

    // Return to the main menu panel.
    public void OnReturnButtonClicked()
    {
        createRoomPanel.SetActive(false);
        joinRoomPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    // Transfer to the create room menu.
    public void OnCreateRoomMenuButtonClicked()
    {
        mainMenuPanel.SetActive(false);
        createRoomPanel.SetActive(true);
    }

    // Transfer to the join room menu and receive room information string.
    public void OnJoinRoomMenuButtonClicked()
    {
        mainMenuPanel.SetActive(false);
        joinRoomPanel.SetActive(true);

        serverConnection.RequestRooms();
        //roomsScrollView.GetComponent<Rooms>().ProcessRoomsString(roomsString);
    }

    // Create room and transfer player to the game scene.
    public void OnCreateRoomButtonClicked()
    {
        if (CheckNameValidity(RoomName) == true)
        {
            roomNameWarning.SetActive(false);
            CreateRoom();
        }
        else
        {
            roomNameWarning.SetActive(true);
        }
    }

    // Message used to create room.
    private string CreateRoomMessage()
    {
        string message = "CreateRoom_" + RoomName + "_" + serverConnection.playerName;
        return message;
    }

    // Send info on new room and receive acknowledgement.
    public void CreateRoom()
    {
        menuState = MenuState.WaitingForCreate;
        string createRoomMessage = CreateRoomMessage();
        serverConnection.SendToServer(createRoomMessage);
    }

    // Load scene with room layout.
    public void LoadRoom()
    {
        SceneManager.LoadScene("Game");
    }

    // Handle player name input from the input field.
    public void OnPlayerNameSubmitted()
    {
        serverConnection.playerName = playerNameInputField.GetComponent<TMP_InputField>().text;
        Debug.Log("Player name: " + serverConnection.playerName);
    }

    // If the room will be created.
    public void OnHostRoomNameSubmitted()
    {
        RoomName = roomNameInputField.GetComponent<TMP_InputField>().text;
        Debug.Log("Host room name: " + RoomName);
    }

    // Show connected label.
    public void ShowConnectedLabel()
    {
        connectedLabel.SetActive(true);
        notConnectedLabel.SetActive(false);
    }

    // Show not connected label.
    public void ShowNotConnectedLabel()
    {
        notConnectedLabel.SetActive(true);
        connectedLabel.SetActive(true);
    }

    public void SendFault()
    {
        serverConnection.SendToServer("Fault");
    }

    public void ShowDisconnectedLabel()
    {
        //menuState = MenuState.Disconnected;
        disconnectedLabel.SetActive(true);
    }

    public void ShowConnectionWarning()
    {
        connectionLabel.SetActive(true);
    }

    public void HideConnectionWarning()
    {
        connectionLabel.SetActive(false);
    }

}
