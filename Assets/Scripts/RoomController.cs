﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading;
using UnityEngine.SceneManagement;

// The class is responsible for a game scene behaviour.
public class RoomController : MonoBehaviour
{

    // Opponent's name label.
    public GameObject opponentsNameLabel;

    public GameObject playersScoreLabel;
    public GameObject opponentsScoreLabel;

    public GameObject playerPlays;
    public GameObject opponentPlays;

    public GameObject playerWon;
    public GameObject opponentWon;

    public GameObject drawLabel;

    public GameObject returnButton;

    public GameObject waitingLabel;
    public GameObject warningLabel;

    public GameObject disconnectedLabel;

    // Determines who is currently playing.
    public enum PlayerState { MePlays, OpponentPlays };
    public PlayerState playerState;

    // Determines the state of the current turn.
    public enum TurnState { FirstPick, SecondPick, NoPick };
    public TurnState turnState;

    public enum StartedRoomState { RoomInactive, WaitingForNameResponse, WaitingForStartResponse, WaitingForPlayersOutcome, WaitingForOpponentsOutcome, WaitingForResults, WaitingForReconnect, RoomActive, Disconnected, ConnectionLost };
    public StartedRoomState startedRoomState;

    public StartedRoomState savedRoomState;

    // Server connection.
    private ServerConnection serverConnection;

    // Tag used to identify the card object.
    [SerializeField]
    private string cardTag = "Card";
    // Name of the card front object.
    [SerializeField]
    private string cardFrontName = "Card_front";
    // Name of the card back object.
    [SerializeField]
    private string cardBackName = "Card_back";

    // Name of the opponent.
    private string opponentName;

    // Id of the first selected card.
    private int firstCardId;
    // Id of the second selected card.
    private int secondCardId;

    private int playersScore;
    private int opponentsScore;

    // Array of the cards game objects.
    private GameObject[] cards;

    // Start is called before the first frame update
    void Start()
    {
        serverConnection = ServerConnection.Instance;
        cards = new GameObject[64];
        for (int i = 0; i < 64; i++)
        {
            cards[i] = GameObject.Find("Card_" + i);
        }

        if (serverConnection.restoringRoom == false)
        {
            LoadBlankRoom();
        }
        else
        {
            serverConnection.restoringRoom = false;
            LoadBlankRoom(); 
            SetupRoom(serverConnection.restoreInfo);
        }

        //serverConnection.StartOpponentsPing();
    }

    void LoadBlankRoom()
    {
        playerPlays.SetActive(false);
        opponentPlays.SetActive(false);
        playerWon.SetActive(false);
        returnButton.SetActive(false);
        opponentWon.SetActive(false);
        drawLabel.SetActive(false);
        waitingLabel.SetActive(false);
        warningLabel.SetActive(false);
        disconnectedLabel.SetActive(false);

        serverConnection.roomController = GameObject.Find("RoomController").GetComponent<RoomController>();
        turnState = TurnState.NoPick;
        startedRoomState = StartedRoomState.RoomInactive;

        serverConnection.roomLoaded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) == true && playerState == PlayerState.MePlays && turnState != TurnState.NoPick && startedRoomState != StartedRoomState.Disconnected && startedRoomState != StartedRoomState.WaitingForReconnect)
        {
            HandleCardClick();
        }

    }

    public void AskForOpponentsName()
    {
        Debug.Log("I AM ASKING FOR AN OPPONENTS NAME.");
        startedRoomState = StartedRoomState.WaitingForNameResponse;
        serverConnection.SendToServer("SendOpponentsName");
    }

    public void ProcessOpponentsName(string name)
    {
        opponentName = name;
        DisplayOpponentsName();
    }

    public void AskWhoStarts()
    {
        Debug.Log("I AM ASKING WHO STARTS.");
        startedRoomState = StartedRoomState.WaitingForStartResponse;
        serverConnection.SendToServer("WhoStarts");
    }

    public void ProcessPlayersStart()
    {
        turnState = TurnState.FirstPick;
        playerState = PlayerState.MePlays;
        playerPlays.SetActive(true);
        startedRoomState = StartedRoomState.RoomActive;
    }

    public void ProcessOpponentsStart()
    {
        turnState = TurnState.FirstPick;
        playerState = PlayerState.OpponentPlays;
        opponentPlays.SetActive(true);
        startedRoomState = StartedRoomState.WaitingForOpponentsOutcome;
    }

    public void OpponentsTurn(string message)
    {
        StartCoroutine(HandleOpponentsTurn(message));
    }

    private IEnumerator HandleOpponentsTurn(string message)
    {
        string header;
        int cardId;
        int imageId;
        int turnNumber;
        int paired;
        int completed;
        ProcessTurnOutcome(message, out header, out cardId, out imageId, out turnNumber, out paired, out completed);

        if (header.Equals("OpponentsTurnOutcome"))
        {
            showCard(cardId, imageId);

            if (turnNumber == 1)
            {
                firstCardId = cardId;
            }
            else if (turnNumber == 2)
            {
                secondCardId = cardId;
                if (paired == 1)
                {
                    IncrementOpponentsScore();
                    if (completed == 1)
                    {
                        startedRoomState = StartedRoomState.WaitingForResults;
                        ReceiveResults();
                    }
                }
                else
                {
                    yield return new WaitForSeconds(2.5f);
                    hideCard(firstCardId);
                    hideCard(secondCardId);
                    playerState = PlayerState.MePlays;
                    turnState = TurnState.FirstPick;
                    ShowPlayerPlays();
                }
            }
        }
    }

    private void IncrementPlayersScore()
    {
        playersScore++;
        playersScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + playersScore;
    }

    private void IncrementOpponentsScore()
    {
        opponentsScore++;
        opponentsScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + opponentsScore;
    }

    // Displays opponent's name in the label field.
    private void DisplayOpponentsName()
    {
        opponentsNameLabel.GetComponent<TextMeshProUGUI>().text = opponentName + " points:";
    }

    // Handles the click on the card object.
    private void HandleCardClick()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.transform != null && hit.transform.tag.Equals(cardTag))
        {
            Debug.Log("Selected card: " + hit.transform.name);

            if (playerState == PlayerState.MePlays && (turnState == TurnState.FirstPick || turnState == TurnState.SecondPick))
            {
                int cardId = hit.transform.GetComponent<CardId>().GetCardId();
                string turnMessage = TurnMessage(cardId);
                startedRoomState = StartedRoomState.WaitingForPlayersOutcome;
                SendTheTurn(turnMessage);
            }

        }
    }

    public void PlayersTurn(string message)
    {
        StartCoroutine(ProcessCardClickResponse(message));
    }

    public IEnumerator ProcessCardClickResponse(string message)
    {
        startedRoomState = StartedRoomState.RoomActive;
        string header;
        int receivedCardId;
        int imageId;
        int turnNumber;
        int paired;
        int completed;
        ProcessTurnOutcome(message, out header, out receivedCardId, out imageId, out turnNumber, out paired, out completed);

        if (header.Equals("TurnOutcome"))
        {
            showCard(receivedCardId, imageId);

            if (turnState == TurnState.FirstPick)
            {
                firstCardId = receivedCardId;
                turnState = TurnState.SecondPick;
            }
            else if (turnState == TurnState.SecondPick)
            {
                secondCardId = receivedCardId;

                if (paired == 1)
                {
                    turnState = TurnState.FirstPick;
                    IncrementPlayersScore();
                    if (completed == 1)
                    {
                        startedRoomState = StartedRoomState.WaitingForResults;
                        ReceiveResults();
                    }
                }
                else
                {
                    turnState = TurnState.NoPick;
                    yield return new WaitForSeconds(2.5f);
                    hideCard(firstCardId);
                    hideCard(secondCardId);
                    playerState = PlayerState.OpponentPlays;
                    startedRoomState = StartedRoomState.WaitingForOpponentsOutcome;
                    ShowOpponentPlays();
                }
            }
        }
    }

    private void ReceiveResults()
    {
        serverConnection.SendToServer("AwaitingResults_AA");
    }

    public void ProcessResults(string message)
    {
        string[] parts = ServerConnection.ParseMessage(message);
        int myScore = int.Parse(parts[1]);
        int opponentScore = int.Parse(parts[2]);

        if (myScore > opponentScore)
        {
            playerWon.SetActive(true);
        }
        else if (myScore == opponentScore)
        {
            drawLabel.SetActive(true);
        }
        else
        {
            opponentWon.SetActive(true);
        }

        serverConnection.completed = true;
        returnButton.SetActive(true);
    }

    public void ReturnButtonClicked()
    {
        SceneManager.LoadScene("Menu");
    }

    // Message about current turn.
    private string TurnMessage(int cardId)
    {
        return "Picked_" + cardId;
    }

    // Sends the turn message to the server.
    private void SendTheTurn(string turnMessage)
    {
        serverConnection.SendToServer(turnMessage);
    }

    private void ProcessTurnOutcome(string message, out string header, out int cardId, out int imageId, out int turnNumber, out int paired, out int completed)
    {
        string[] parts = ServerConnection.ParseMessage(message);
        header = parts[0];
        cardId = int.Parse(parts[1]);
        imageId = int.Parse(parts[2]);
        turnNumber = int.Parse(parts[3]);
        if (turnNumber == 2)
        {
            paired = int.Parse(parts[4]);
        }
        else
        {
            paired = -1;
        }
        completed = int.Parse(parts[5]);
    }

    public void SetupRoom(string setupMessage)
    {
        string[] parts = ServerConnection.ParseMessage(setupMessage);
        ProcessOpponentsName(parts[1]);
        playersScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + parts[2];
        opponentsScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + parts[3];
        playersScore = int.Parse(parts[2]);
        opponentsScore = int.Parse(parts[3]);

        if (parts[4].Equals("PlayerPlays"))
        {
            playerState = PlayerState.MePlays;
            ShowPlayerPlays();
            startedRoomState = StartedRoomState.RoomActive;
        }
        else if (parts[4].Equals("OpponentPlays"))
        {
            playerState = PlayerState.OpponentPlays;
            ShowOpponentPlays();
            startedRoomState = StartedRoomState.WaitingForOpponentsOutcome;
        }

        if (parts[5].Equals("FirstPick"))
        {
            turnState = TurnState.FirstPick;
        }
        else if (parts[5].Equals("SecondPick"))
        {
            turnState = TurnState.SecondPick;
            int firstPickedImage = int.Parse(parts[6]);
            int firstPickedIndex = int.Parse(parts[7]);
            showCard(firstPickedIndex, firstPickedImage);
            firstCardId = firstPickedIndex;
        }

        int index = 0;
        for (int i = 8; i <= 71; i++)
        {
            int id = int.Parse(parts[i]);
            if (id != -1)
            {
                showCard(index, id);
            }
            index++;
        }
    }

    private void showCard(int cardId, int imageId)
    {
        GameObject cardBack = cards[cardId].transform.Find(cardBackName).gameObject;
        cardBack.SetActive(false);

        cards[cardId].GetComponent<BoxCollider2D>().enabled = false;

        GameObject cardFront = cards[cardId].transform.Find(cardFrontName).gameObject;
        cardFront.AddComponent<SpriteRenderer>();
        SpriteRenderer spriteRenderer = cardFront.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Resources.Load<Sprite>("image" + imageId);
    }

    private void hideCard(int cardId)
    {
        GameObject cardFront = cards[cardId].transform.Find(cardFrontName).gameObject;
        SpriteRenderer spriteRenderer = cardFront.GetComponent<SpriteRenderer>();
        Destroy(spriteRenderer);
        //cardFront.SetActive(false);

        cards[cardId].GetComponent<BoxCollider2D>().enabled = true;

        GameObject cardBack = cards[cardId].transform.Find(cardBackName).gameObject;
        cardBack.SetActive(true);
    }

    public void ShowPlayerPlays()
    {
        playerPlays.SetActive(true);
        opponentPlays.SetActive(false);
    }

    public void ShowOpponentPlays()
    {
        opponentPlays.SetActive(true);
        playerPlays.SetActive(false);
    }

    public void ShowWaitingLabel()
    {
        waitingLabel.SetActive(true);
    }

    public void HideWaitingLabel()
    {
        waitingLabel.SetActive(false);
    }

    public void ShowWarningLabel()
    {
        warningLabel.SetActive(true);
    }

    public void HideWarningLabel()
    {
        warningLabel.SetActive(false);
    }

    public void ShowDisconnected()
    {
        startedRoomState = StartedRoomState.Disconnected;
        disconnectedLabel.SetActive(true);
        waitingLabel.SetActive(false);
    }

}
