﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The class holds the card's id number;
public class CardId : MonoBehaviour
{

    // Id number.
    public int id;
    
    public int GetCardId()
    {
        return id;
    }

}