﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using TMPro;
//using System.Threading;

//// The class is responsible for a game scene behaviour.
//public class RoomController : MonoBehaviour
//{

//    // Opponent's name label.
//    public GameObject opponentsNameLabel;

//    public GameObject playersScoreLabel;
//    public GameObject opponentsScoreLabel;

//    public GameObject playerPlays;
//    public GameObject opponentPlays;

//    public GameObject playerWon;
//    public GameObject opponentWon;

//    public GameObject drawLabel;

//    public GameObject returnButton;

//    // Determines who is currently playing.
//    private enum PlayerState { MePlays, OpponentPlays };
//    private PlayerState playerState;

//    // Determines the state of the current turn.
//    private enum TurnState { FirstPick, SecondPick, NoPick };
//    private TurnState turnState;

//    // Server connection.
//    private ServerConnection serverConnection;

//    // Tag used to identify the card object.
//    [SerializeField]
//    private string cardTag = "Card";
//    // Name of the card front object.
//    [SerializeField]
//    private string cardFrontName = "Card_front";
//    // Name of the card back object.
//    [SerializeField]
//    private string cardBackName = "Card_back";

//    // Name of the opponent.
//    private string opponentName;

//    // Id of the first selected card.
//    private int firstCardId;
//    // Id of the second selected card.
//    private int secondCardId;

//    private int playersScore;
//    private int opponentsScore;

//    // Array of the cards game objects.
//    private GameObject[] cards;



//    private bool startOpponentsTurn;




//    // Start is called before the first frame update
//    void Start()
//    {
//        serverConnection = ServerConnection.Instance;
//        cards = GameObject.FindGameObjectsWithTag(cardTag);
//        playerPlays.SetActive(false);
//        opponentPlays.SetActive(false);
//        playerWon.SetActive(false);
//        returnButton.SetActive(false);
//        opponentWon.SetActive(false);
//        drawLabel.SetActive(false);
//        startOpponentsTurn = false;

//        string startMessage = serverConnection.ReceiveFromServer();
//        SetUpRoom();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (startOpponentsTurn == true && playerState == PlayerState.OpponentPlays)
//        {
//            startOpponentsTurn = false;
//            HandleOpponentsTurn();
//        }
//        if (Input.GetMouseButtonDown(0) == true && playerState == PlayerState.MePlays)
//        {
//            HandleCardClick();
//        }
        
//    }

//    // Sets up the room.
//    private void SetUpRoom()
//    {
//        serverConnection.SendToServer("SendOpponentsName");
//        string opponentsNameMessage = serverConnection.ReceiveFromServer();
//        string[] words2 = ServerConnection.ParseMessage(opponentsNameMessage);
//        opponentName = words2[1];
//        DisplayOpponentsName();

//        serverConnection.SendToServer("WhoStarts");
//        string roomInfo = serverConnection.ReceiveFromServer();
//        string[] words = ServerConnection.ParseMessage(roomInfo);
//        if (words[0].Equals("MeStarts"))
//        {
//            turnState = TurnState.FirstPick;
//            playerState = PlayerState.MePlays;
//            playerPlays.SetActive(true);
//            startOpponentsTurn = false;
//        }
//        else if (words[0].Equals("OpponentStarts"))
//        {
//            turnState = TurnState.FirstPick;
//            playerState = PlayerState.OpponentPlays;
//            opponentPlays.SetActive(true);
//            startOpponentsTurn = true;
//            //HandleOpponentsTurn();
//        }
//    }

//    private void HandleOpponentsTurn()
//    {
//        string header;
//        int cardId;
//        int imageId;
//        int turnNumber;
//        int paired;
//        int completed;
//        ReceiveTurnOutcome(out header, out cardId, out imageId, out turnNumber, out paired, out completed);

//        if (header.Equals("OpponentsTurnOutcome"))
//        {
//            showCard(cardId, imageId);

//            if (turnNumber == 1)
//            {
//                firstCardId = cardId;
//                startOpponentsTurn = true;
//                //HandleOpponentsTurn();
//            }
//            else if (turnNumber == 2)
//            {
//                secondCardId = cardId;
//                if (paired == 1)
//                {
//                    IncrementOpponentsScore();
//                    startOpponentsTurn = true;
//                    //HandleOpponentsTurn();
//                    if (completed == 1)
//                    {
//                        //ReceiveResults();
//                    }
//                }
//                else
//                {
//                    hideCard(firstCardId);
//                    hideCard(secondCardId);
//                    playerState = PlayerState.MePlays;
//                    turnState = TurnState.FirstPick;
//                }
//            }
//        }
//    }

//    private void IncrementPlayersScore()
//    {
//        playersScore++;
//        playersScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + playersScore;
//    }

//    private void IncrementOpponentsScore()
//    {
//        opponentsScore++;
//        opponentsScoreLabel.GetComponent<TextMeshProUGUI>().text = "" + opponentsScore;
//    }

//    // Displays opponent's name in the label field.
//    private void DisplayOpponentsName()
//    {
//        opponentsNameLabel.GetComponent<TextMeshProUGUI>().text = opponentName + " points:";
//    }

//    // Handles the click on the card object.
//    private void HandleCardClick()
//    {
//        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
//        if (hit.transform.name != null)
//        {
//            Debug.Log("Selected card: " + hit.transform.name);

//            if (playerState == PlayerState.MePlays && (turnState == TurnState.FirstPick || turnState == TurnState.SecondPick))
//            {
//                int cardId = hit.transform.GetComponent<CardId>().GetCardId();
//                string turnMessage = TurnMessage(cardId);
//                SendTheTurn(turnMessage);

//                string header;
//                int receivedCardId;
//                int imageId;
//                int turnNumber;
//                int paired;
//                int completed;
//                ReceiveTurnOutcome(out header, out receivedCardId, out imageId, out turnNumber, out paired, out completed);

//                if (header.Equals("TurnOutcome"))
//                {
//                    showCard(cardId, imageId);

//                    if (turnState == TurnState.FirstPick)
//                    {
//                        firstCardId = cardId;
//                        turnState = TurnState.SecondPick;
//                    }
//                    else if (turnState == TurnState.SecondPick)
//                    {
//                        secondCardId = cardId;

//                        if (paired == 1)
//                        {
//                            turnState = TurnState.FirstPick;
//                            IncrementPlayersScore();
//                            if (completed == 1)
//                            {
//                                ReceiveResults();
//                            }
//                        }
//                        else
//                        {
//                            hideCard(firstCardId);
//                            hideCard(secondCardId);
//                            playerState = PlayerState.OpponentPlays;
//                            startOpponentsTurn = true;
//                            //HandleOpponentsTurn();
//                        }
//                    }
//                }
//            }

//        }
//    }

//    private void ReceiveResults()
//    {
//        serverConnection.SendMessage("AwaitingResults");
//        string message = serverConnection.ReceiveFromServer();
//        string[] parts = ServerConnection.ParseMessage(message);
//        int myScore = int.Parse(parts[1]);
//        int opponentScore = int.Parse(parts[2]);

//        if (myScore > opponentScore)
//        {
//            playerWon.SetActive(true);
//        }
//        else if (myScore == opponentScore)
//        {
//            drawLabel.SetActive(true);
//        }
//        else
//        {
//            opponentWon.SetActive(true);
//        }
//    }

//    // Message about current turn.
//    private string TurnMessage(int cardId)
//    {
//        return "Picked_" + cardId;
//    }

//    // Sends the turn message to the server.
//    private void SendTheTurn(string turnMessage)
//    {
//        serverConnection.SendToServer(turnMessage);
//    }

//    // Message format: TurnOutcome_<cardId>_<imageId>_[1,2 - first or second turn]_[0 if not paired, 1 if paired - only to second turn]_[1 if game is completed, 0 if not].
//    private void ReceiveTurnOutcome(out string header, out int cardId, out int imageId, out int turnNumber, out int paired, out int completed)
//    {
//        string message = serverConnection.ReceiveFromServer();
//        string[] parts = ServerConnection.ParseMessage(message);
//        header = parts[0];
//        cardId = int.Parse(parts[1]);
//        imageId = int.Parse(parts[2]);
//        turnNumber = int.Parse(parts[3]);
//        if (turnNumber == 1)
//        {
//            paired = int.Parse(parts[4]);
//        }
//        else
//        {
//            paired = -1;
//        }
//        completed = int.Parse(parts[5]);
//    }

//    private void showCard(int cardId, int imageId)
//    {
//        GameObject cardBack = cards[cardId].transform.Find(cardBackName).gameObject;
//        cardBack.SetActive(false);

//        cards[cardId].GetComponent<BoxCollider2D>().enabled = false;

//        GameObject cardFront = cards[cardId].transform.Find(cardFrontName).gameObject;
//        cardFront.AddComponent<SpriteRenderer>();
//        SpriteRenderer spriteRenderer = cardFront.GetComponent<SpriteRenderer>();
//        spriteRenderer.sprite = Resources.Load<Sprite>("image" + imageId);
//        //spriteRenderer.color = new Color(255, 0, 0);
//    }

//    private void hideCard(int cardId)
//    {
//        GameObject cardFront = cards[cardId].transform.Find(cardFrontName).gameObject;
//        SpriteRenderer spriteRenderer = cardFront.GetComponent<SpriteRenderer>();
//        Destroy(spriteRenderer);
//        cardFront.SetActive(false);

//        cards[cardId].GetComponent<BoxCollider2D>().enabled = true;

//        GameObject cardBack = cards[cardId].transform.Find(cardBackName).gameObject;
//        cardBack.SetActive(true);
//    }

//}
