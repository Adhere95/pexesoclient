﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine.SceneManagement;
using System;

// The class is responsible for a server connection.
public class ServerConnection : MonoBehaviour
{
    // Only instance of the class.
    private static ServerConnection _instance;

    // Instance property.
    public static ServerConnection Instance
    {
        get { return _instance; }
    }

    // Input field for the server IP.
    public GameObject serverIpInputField;
    // Input field for the server port.
    public GameObject serverPortInputField;

    // Server IP string from the input field.
    private string serverIp;
    // Server port string from the input field.
    private string serverPort;

    // Socket of the client.
    private Socket clientSocket;

    // Main menu ui controller.
    private MainMenuController mainMenuController;
    public GameObject mainMenuControllerObject;

    // Rooms script.
    private Rooms roomsScript;
    public GameObject roomsScriptObject;

    // Room controller.
    public RoomController roomController;

    Queue<string> stringQueue = new Queue<string>();
    // Thread message received from a server.
    private string threadMessage;
    // Defines if the message has already been received.
    public bool isMessageReceived;

    public bool completed;
    public bool disconnected;

    public bool restoringRoom;
    public string restoreInfo;

    public string playerName;
    public string opponentName;

    public bool pingReceived;
    public bool opponentsPingReceived;

    private IEnumerator sendPingLoop;
    private IEnumerator opponentsPing;

    private IEnumerator activityTimer;

    private bool unavailableSent = false;
    public bool showReconnect = false;

    private Thread thread;

    public bool roomLoaded = false;

    private void Update()
    {
        if (disconnected == true)
        {
            if (SceneManager.GetActiveScene().name.Equals("Menu"))
            {
                mainMenuController.ShowDisconnectedLabel();
                //StopAllCoroutines();
            }
            else if (SceneManager.GetActiveScene().name.Equals("Game"))
            {
                roomController.ShowDisconnected();
                //StopAllCoroutines();
            }
            StopCoroutine(sendPingLoop);
            StopCoroutine(opponentsPing);
        }
        //if (isMessageReceived == true)
        if (true)
        {
            //HandleMessage(threadMessage);
            string[] array = stringQueue.ToArray();
            //            foreach (string message in array)
            //            {
            //                HandleMessage(message);
            //                stringQueue.Dequeue();
            //           }

            while (stringQueue.ToArray().Length > 0)
            {
                string message = stringQueue.Dequeue();
                HandleMessage(message);
            }

            isMessageReceived = false;
            //stringQueue.Clear();
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(this);
        completed = false;
        disconnected = false;
        restoringRoom = false;
    }

    // Start is called before the first frame update
    // Initialization.
    void Start()
    {
        mainMenuController = mainMenuControllerObject.GetComponent<MainMenuController>();
        isMessageReceived = false;
        roomsScript = roomsScriptObject.GetComponent<Rooms>();
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == 0)
        {
            serverIpInputField = GameObject.Find("ServerIpInput");
            serverPortInputField = GameObject.Find("PortNumberInput");
            mainMenuControllerObject = GameObject.Find("MainMenuController");
            roomsScriptObject = GameObject.Find("Scroll View_Rooms");
            roomsScript = roomsScriptObject.GetComponent<Rooms>();
            mainMenuController = mainMenuControllerObject.GetComponent<MainMenuController>();


        }
    }

    // Method used to parse incoming message from the server.
    public static string[] ParseMessage(string message)
    {
        string[] parts = message.Split('_');
        return parts;
    }

    // Message used to connect to the server.
    private string ConnectMessage()
    {
        string message = "Connect_";
        message += playerName;

        return message;
    }

    // Try the connection to the server.
    public bool ConnectToServer()
    {
        //try
        //{
        //    //IPAddress ipAddress = IPAddress.Parse("192.168.80.131");
        //    IPAddress ipAddress = IPAddress.Parse(serverIp);
        //    //IPEndPoint serverPoint = new IPEndPoint(ipAddress, int.Parse("10000"));
        //    IPEndPoint serverPoint = new IPEndPoint(ipAddress, int.Parse(serverPort));
        //    clientSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        //    try
        //    {
        //        string connectMessage = ConnectMessage();

        //        clientSocket.Connect(serverPoint);
        //        SendToServer(connectMessage);

        //        ReceiveThread();
        //        //StartCoroutine("SendPingLoop");
        //        return true;
        //    }
        //    catch
        //    {
        //        Debug.Log("Connection failed.");
        //        return false;
        //    }
        //}
        //catch
        //{
        //    Debug.Log("Socket failed.");
        //    return false;
        //}

        try
        {
            //IPAddress ipAddress = IPAddress.Parse("192.168.80.131");
            IPAddress ipAddress = IPAddress.Parse(serverIp);
            //IPEndPoint serverPoint = new IPEndPoint(ipAddress, int.Parse("10000"));
            IPEndPoint serverPoint = new IPEndPoint(ipAddress, int.Parse(serverPort));
            clientSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Connect using a timeout (5 seconds)

            IAsyncResult result = clientSocket.BeginConnect(ipAddress, int.Parse(serverPort), null, null);

            bool success = result.AsyncWaitHandle.WaitOne(2000, true);

            if (clientSocket.Connected)
            {
                clientSocket.EndConnect(result);
            }
            else
            {
                // NOTE, MUST CLOSE THE SOCKET

                clientSocket.Close();
                throw new ApplicationException("Failed to connect server.");

            }

            try
            {
                string connectMessage = ConnectMessage();
                //clientSocket.Connect(serverPoint);
                SendToServer(connectMessage);

                ReceiveThread();
                //StartCoroutine("SendPingLoop");
                return true;
            }
            catch
            {
                Debug.Log("Connection failed.");
                return false;
            }
        }
        catch
        {
            Debug.Log("Socket failed.");
            return false;
        }
    }

    public void StartPingLoop()
    {
        Debug.Log("STARTING A PING COROUTINE.");
        //StartCoroutine("SendPingLoop");
        sendPingLoop = SendPingLoop();
        StartCoroutine(sendPingLoop);
    }

    private IEnumerator SendPingLoop()
    {
        bool pingLoop = true;
        while (pingLoop == true)
        {
            pingReceived = false;
            //opponentsPingReceived = false;
            SendToServer("Ping_Client");
            yield return new WaitForSeconds(5);
            if (pingReceived == false)
            {
                clientSocket.Close();
                StopCoroutine(opponentsPing);
                thread.Abort();
                Debug.Log("Thread aborted.");
                StartCoroutine("ReconnectToServer");
                pingLoop = false;
                Debug.Log("Ping not received");
                showReconnect = true;
                if (SceneManager.GetActiveScene().name.Equals("Menu"))
                {
                    mainMenuController.ShowConnectionWarning();
                }
                else if (SceneManager.GetActiveScene().name.Equals("Game"))
                {
                    roomController.ShowWarningLabel();
                }
            }
            else
            {
                if (SceneManager.GetActiveScene().name.Equals("Menu"))
                {
                    mainMenuController.HideConnectionWarning();
                }
                else if (SceneManager.GetActiveScene().name.Equals("Game"))
                {
                    roomController.HideWarningLabel();
                }
            }

            //yield return new WaitForSeconds(2);

            //if (opponentsPingReceived == false && SceneManager.GetActiveScene().name.Equals("Game") && roomController.startedRoomState != RoomController.StartedRoomState.RoomInactive)
            //{
            //    SendToServer("OpponentUnavailable_AA");
            //    roomController.waitingLabel.SetActive(true);
            //}
            //else if (opponentsPingReceived == true && SceneManager.GetActiveScene().name.Equals("Game") && roomController.startedRoomState != RoomController.StartedRoomState.RoomInactive)
            //{
            //    roomController.waitingLabel.SetActive(false);
            //}
        }
    }

    public void StartOpponentsPing()
    {
        Debug.Log("STARTING AN OPPONENTS PING COROUTINE.");
        //StartCoroutine("OpponentsPing");
        opponentsPing = OpponentsPing();
        StartCoroutine(opponentsPing);
    }

    public IEnumerator OpponentsPing()
    {
        while (true)
        {
            opponentsPingReceived = false;
            yield return new WaitForSeconds(7);

            if (opponentsPingReceived == false && SceneManager.GetActiveScene().name.Equals("Game") && roomController.startedRoomState != RoomController.StartedRoomState.RoomInactive && unavailableSent == false)
            {
                SendToServer("OpponentUnavailable_Client");
                //roomController.waitingLabel.SetActive(true);
                unavailableSent = true;

                roomController.savedRoomState = roomController.startedRoomState;
                roomController.startedRoomState = RoomController.StartedRoomState.WaitingForReconnect;
                roomController.ShowWaitingLabel();

                StartActivityTimer();

                break;
            }
            else if (opponentsPingReceived == true && SceneManager.GetActiveScene().name.Equals("Game") && roomController.startedRoomState != RoomController.StartedRoomState.RoomInactive)
            {
                roomController.waitingLabel.SetActive(false);
                unavailableSent = false;
            }
        }
    }

    private IEnumerator ReconnectToServer()
    {
        SceneManager.LoadScene("Menu");
        mainMenuController.menuState = MainMenuController.MenuState.WaitingForConnection;
        bool connect = false;
        while (connect == false)
        {
            connect = ConnectToServer();
            yield return new WaitForSeconds(5);
        }
    }

    public void StartActivityTimer()
    {
        activityTimer = ActivityTimer();
        StartCoroutine(activityTimer);
    }

    private IEnumerator ActivityTimer()
    {
        yield return new WaitForSeconds(45);
        SendToServer("LongWaiting_Client");
        Debug.Log("Opponent inactive for a long time.");
    }

    // Sends a message to the server.
    public void SendToServer(string message)
    {
        //string finalMessage = messagesCounter + "_PexesoClient_" + message + "\n";
        string finalMessage = "5_PexesoClient_" + message + "\n";
        byte[] messageData = Encoding.Default.GetBytes(finalMessage);
        Debug.Log("Sending a message to a server: " + finalMessage);

        try
        {
            clientSocket.Send(messageData);
        }
        catch
        {
            Debug.Log("Send failed.");
        }
    }

    // Starts a thread to receive a message from server.
    public void ReceiveThread()
    {
        thread = new Thread(ThreadReceive);
        thread.Start();
    }

    private void ThreadReceive()
    {
        while (true)
        {
            //if (clientSocket.Available > 0)
            //{


            string message = ReceiveFromServer();
            threadMessage = message;

            if (message.Equals(""))
            {
                Debug.Log("Disconnected from server.");
                disconnected = true;
                Thread.CurrentThread.Abort();
            }
            else
            {
                stringQueue.Enqueue(message);
                isMessageReceived = true;
            }



            //}
        }
    }

    // Receives a message from the server.
    public void HandleMessage(string message)
    {
        string messageBody = ProcessMessage(message);
        string[] parts = ParseMessage(messageBody);

        if (parts[0].Equals("Ping"))
        {
            Debug.Log("Ping");
            pingReceived = true;
        }
        else if (parts[0].Equals("ClientRecognized"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForConnection)
            //{
            mainMenuController.panelReconnect.SetActive(false);
            mainMenuController.ShowConnectedLabel();
            mainMenuController.TransferToMainMenu();
            StartPingLoop();
            //}
        }
        else if (parts[0].Equals("Recognized"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForConnection)
            //{
            Debug.Log("Recognized client");
            restoringRoom = true;
            restoreInfo = string.Copy(messageBody);
            disconnected = false;
            mainMenuController.LoadRoom();
            StartPingLoop();
            StartOpponentsPing();
            //}
        }
        else if (parts[0].Equals("CreatingRoom"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForCreate)
            //{
            mainMenuController.LoadRoom();
            //}
        }
        else if (parts[0].Equals("RoomsInfo"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForInfo)
            //{
            roomsScript.ProcessRoomsString(message);
            //}
        }
        else if (parts[0].Equals("JoinedRoom"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForJoin)
            //{
            roomsScript.TransferToRoom();
            //}
        }
        else if (parts[0].Equals("RoomFull"))
        {
            //if (mainMenuController.menuState == MainMenuController.MenuState.WaitingForJoin)
            //{
            roomsScript.ShowWarning();
            //}
        }
        else if (parts[0].Equals("RoomStarted"))
        {
            StartCoroutine("WaitForLoad");

            //if (roomController.startedRoomState == RoomController.StartedRoomState.RoomInactive)
            //{
            //    roomController.AskForOpponentsName();
            //    roomController.HideWaitingLabel();
            //}

            StartOpponentsPing();
        }
        else if (parts[0].Equals("OpponentsName"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForNameResponse)
            //{
            roomController.ProcessOpponentsName(parts[1]);
            roomController.AskWhoStarts();
            //}
        }
        else if (parts[0].Equals("MeStarts"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForStartResponse)
            //{
            roomController.ProcessPlayersStart();
            //}
        }
        else if (parts[0].Equals("OpponentStarts"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForStartResponse)
            //{
            roomController.ProcessOpponentsStart();
            //}
        }
        else if (parts[0].Equals("TurnOutcome"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForPlayersOutcome && roomController.playerState == RoomController.PlayerState.MePlays)
            //{
            roomController.PlayersTurn(messageBody);
            //}
        }
        else if (parts[0].Equals("OpponentsTurnOutcome"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForOpponentsOutcome && roomController.playerState == RoomController.PlayerState.OpponentPlays)
            //{
            roomController.OpponentsTurn(messageBody);
            //}
        }
        else if (parts[0].Equals("Results"))
        {
            //if (roomController.startedRoomState == RoomController.StartedRoomState.WaitingForResults)
            //{
            roomController.ProcessResults(messageBody);
            StopCoroutine(opponentsPing);
            //}
        }
        else if (parts[0].Equals("OpponentDisconnected"))
        {
            roomController.savedRoomState = roomController.startedRoomState;
            roomController.startedRoomState = RoomController.StartedRoomState.WaitingForReconnect;

            StopCoroutine(opponentsPing);
            opponentsPingReceived = false;

            roomController.ShowWaitingLabel();
        }
        else if (parts[0].Equals("OpponentReconnected"))
        {
            StopCoroutine(activityTimer);

            roomController.startedRoomState = roomController.savedRoomState;
            roomController.HideWaitingLabel();

            StartOpponentsPing();

            unavailableSent = false;
        }
        else if (parts[0].Equals("OpponentsPing"))
        {
            Debug.Log("Opponents ping");
            opponentsPingReceived = true;
        }
        else
        {
            StopAllCoroutines();
            thread.Abort();
            clientSocket.Close();
            if (SceneManager.GetActiveScene().name.Equals("Menu"))
            {
                mainMenuController.ShowDisconnectedLabel();
            }
            else if (SceneManager.GetActiveScene().name.Equals("Game"))
            {
                roomController.ShowDisconnected();
                roomController.startedRoomState = RoomController.StartedRoomState.Disconnected;
            }
        }
    }

    public IEnumerator WaitForLoad()
    {
        while (roomLoaded == false)
        {
            yield return null;
        }

        roomLoaded = false;

        //if (roomController.startedRoomState == RoomController.StartedRoomState.RoomInactive)
        //{
        roomController.AskForOpponentsName();
        //roomController.HideWaitingLabel();
        //}
    }

    // Receives a message from the server.
    public string ReceiveFromServer()
    {
        byte[] data = new byte[1024];

        int dataLength = 0;
        try
        {
            dataLength = clientSocket.Receive(data, 1024, SocketFlags.None);
        }
        catch
        {

        }

        string message = "";
        for (int i = 0; i < 1024; i++)
        {
            char character = (char)data[i];
            if (data[i] != 0)
            {
                if (character == '\n')
                {
                    Debug.Log("New line character was received.");
                    break;
                }
                message += (char)data[i];
            }
        }

        Debug.Log("Message length: " + dataLength);
        //string message = Encoding.Default.GetString(data, 0, dataLength);
        Debug.Log("Encoded message: " + message);
        message = ProcessMessage(message);
        return message;
    }

    // Message format is: <counter>_PexesoServer_<MessageType>_<data>/n.
    public string ProcessMessage(string message)
    {
        message = message.Trim();
        string pattern = "^[0-9]+_PexesoServer_";
        string finalMessage = Regex.Replace(message, pattern, "");
        return finalMessage;
    }

    // Message used to request rooms info.
    private string RequestRoomsMessage()
    {
        string message = "SendRooms_Client";
        return message;
    }

    // The method sends the requst to get rooms data.
    public void RequestRooms()
    {
        mainMenuController.menuState = MainMenuController.MenuState.WaitingForInfo;
        string message = RequestRoomsMessage();
        Debug.Log("Request rooms message: " + message);
        //byte[] messageData = Encoding.Default.GetBytes(message);
        // clientSocket.Send(messageData);
        SendToServer(message);
    }

    // If the server IP is submitted into the input field.
    public void OnIpSubmitted()
    {
        serverIp = serverIpInputField.GetComponent<TMP_InputField>().text;
        Debug.Log("Server IP: " + serverIp);
    }

    // If the server port is submitted into the input field.
    public void OnPortSubmitted()
    {
        serverPort = serverPortInputField.GetComponent<TMP_InputField>().text;
        Debug.Log("Server port: " + serverPort);
    }

    public void DisconnectFromServer()
    {
        SendToServer("Disconnect_");
    }

}
